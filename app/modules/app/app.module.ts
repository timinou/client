import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from '@angular/http';

import { AppRoutingModule }     from 'app/modules/routing/app-routing.module';

import { AppComponent }         from 'app/components/app/app.component';
import { DashboardComponent }   from 'app/components/dashboard/dashboard.component';
import { HeroDetailComponent }  from 'app/components/hero-details/hero-detail.component';
import { HeroesComponent }      from 'app/components/heroes/heroes.component';
import { HeroService }          from 'app/services/hero/hero.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroDetailComponent,
    HeroesComponent
  ],
  providers: [ HeroService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
